package com.zuitt.activity_4;

public class Main {
  public static void main(String[] args) {
    User user1 = new User();
    Course course1 = new Course();

    user1.setName("Aljohn Percano");
    user1.setAge(26);
    user1.setEmail("alj@gmail.com");
    user1.setAddress("Nasugbu, Batangas");

    course1.setName("Game Development");
    course1.setDescription("developing games internally for a single platform or console");

    System.out.println("Hi! I'm "+user1.getName()+". I'm "+user1.getAge()+" years old. You can reach me via my email: "+user1.getEmail()+". When I'm off work, I can be found at my house in "+user1.getAddress());

    System.out.println("Welcome to the course "+course1.getName()+". This course can be described as "+course1.getDescription()+". Your instructor for this course is "+user1.getName()+". Enjoy and have fun! Lets build games!");
  }
}


